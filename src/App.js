import logo from "./logo.svg";
import "./App.css";
import React from "react";
import ReactDOM from "react-dom";
import { register } from "./RegisterService";

class PersonList extends React.Component {
  state = {
    login: "",
    password: "",
  };

  handleLoginChange = (event) => {
    this.setState({ login: event.target.value });
  };

  handlePasswordChange = (event) => {
    this.setState({ password: event.target.value });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    const user = {
      login: this.state.login,
      password: this.state.password,
    };

    var response = register(user);
    console.log(response)
  };

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Login:
            <input type="text" name="name" onChange={this.handleLoginChange} />
          </label>
          <label>
            Password:
            <input
              type="text"
              name="name"
              onChange={this.handlePasswordChange}
            />
          </label>
          <button type="submit">Submit</button>
        </form>
      </div>
    );
  }
}

export default PersonList;
